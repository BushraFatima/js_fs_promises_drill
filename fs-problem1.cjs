const fs = require('fs');

function createFolder(folder) {

    return new Promise((resolve,reject) => {
        fs.mkdir(`${folder}`, err => {

            if(err) {
                reject(err);
            } else {
                resolve(console.log("Folder created"));
            } 
        });
    });
}

function createFiles(folder, count) {

    return new Promise((resolve,reject) => {

        for(let index = 1; index <= count; index++) {

            fs.writeFile(`${folder}/jsonFile${index}.json`, "{name: bushra, color: red}", (err) => {
                
                if (err) {
                    reject(err);
                } else {
                    resolve(console.log("File created"));
                } 
            });
        }
    });
}

function deleteFile(folder, count) {

    return new Promise((resolve,reject) => {

        for(let index = 1; index <= count; index++) {
            fs.unlink(`${folder}/jsonFile${index}.json`, (err) => {

                if(err) {
                    reject(err);
                } else {
                    resolve(console.log('File deleted'));
                } 
            });
        }
    });
}

function fsProblem1(folder, count) {

    createFolder(folder)
    .then(() => { 
        return createFiles(folder, count);
    })
    .then(() => {
        return deleteFile(folder, count);
    })
    .catch((err) => {
        console.log((err));
    })
}

module.exports = fsProblem1;