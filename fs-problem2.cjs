const fs = require('fs');

function fileRead(filePath) {

    return new Promise((resolve, reject) => {
        
        fs.readFile(filePath, 'utf8', (err, data) => {

            if (err) {
                reject(err);
            } else {
                console.log('File Read.');
                resolve(data);
            }
        });
    });
}

function doUpper(content) {

    const upperFile =  '/home/bf/Desktop/Promises-Drill-1/upper.txt';
    const modifiedData = content.toUpperCase();

    return new Promise((resolve, reject) => {
        
        fs.writeFile(upperFile, modifiedData, (err) => {

            if(err) {
                reject(err);
            } else {
                console.log('File created');
                fs.readFile(upperFile, 'utf8', (err, data) => {

                    if (err) {
                        reject(err);
                    } else {
                        resolve(data);
                    }
                });
            }
        });
    });
}

function doLowerSplit(content) {

    return new Promise((resolve, reject) => {
        const lowerSplitFile = '/home/bf/Desktop/Promises-Drill-1/lower&split.txt';
        let modifiedData = content.toLowerCase();
        modifiedData = modifiedData.split('. ');
        modifiedData = modifiedData.join('.\n');

        fs.writeFile(lowerSplitFile, modifiedData, (err) => {

            if(err) {
                reject(err);
            } else {
                console.log('File created');

                fs.readFile(lowerSplitFile, 'utf8', (err, data) => {

                    if (err) {
                        reject(err);
                    } else {
                        resolve(data);
                    }
                });
            }
        });
    });
}

function doSort(content) {

    return new Promise((resolve, reject) => {
        const sortFile = '/home/bf/Desktop/Promises-Drill-1/sort.txt';
        let modifiedData = content.split('\n');
        modifiedData = (modifiedData.sort()).slice(3);
        modifiedData = modifiedData.join('\n');

        fs.writeFile(sortFile, modifiedData, (err) => {

            if(err) {
                reject(err);
            } else {
                console.log('File created');
                resolve(console.log('All files created.'));
            }
        });
    });
}

function deleteFile(namesFile) {

    fs.readFile(namesFile, 'utf8', (err, data) => {

        if(err) {
            console.log(err);
        } else {
            
            return new Promise((resolve,reject) => {
                let names = data.split('\n');
                let folder = '/home/bf/Desktop/Promises-Drill-1';
        
                for(let index = 0; index < names.length; index++) {
        
                    fs.unlink(`${folder}/${names[index]}`, (err) => {
        
                        if(err) {
                            reject(err);
                        } else {
                            resolve(console.log(`${names[index]} file deleted`));
                        } 
                    });
                }
            });
        }
    }); 
}

function fsProblem2(filePath) {

    const fileNames = '/home/bf/Desktop/Promises-Drill-1/namesFile.txt';

    fileRead(filePath)
    .then((result1) => {

        fs.appendFile(fileNames, 'upper.txt\n', (err) => {

            if(err) {
                console.log(err);
            } else {
                console.log('Filename added.');
            }
        });

        return doUpper(result1);

    })
    .then((result2) => {

        fs.appendFile(fileNames, 'lower&split.txt\n', (err) => {

            if(err) {
                console.log(err);
            } else {
                console.log('Filename added.');
            }
        });

        return doLowerSplit(result2);

    })
    .then((result3) => {

        fs.appendFile(fileNames, 'sort.txt', (err) => {
            
            if(err) {
                console.log(err);
            } else {
                console.log('Filename added.');
            }
        });

        return doSort(result3);

    })
    .then(() => {
        return deleteFile(fileNames);
    })
    .catch((err) => {
        console.log((err));
    })
}

module.exports = fsProblem2;